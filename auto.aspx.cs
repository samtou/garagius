﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace WebApplication3
{
	public partial class WebForm3 : System.Web.UI.Page
	{
		SqlConnection cnn;
		string connectionString = "Data Source=localhost;Initial Catalog=GMAO; User ID=admin; Password=Asse1254; Integrated Security=SSPI;";

		protected void Page_Load(object sender, EventArgs e)
		{

			if (this.Request.Params.Get("id") != null)
			{
				cnn = new SqlConnection(connectionString);
				cnn.Open();
				SqlCommand cmd = new SqlCommand();
				cmd.CommandText = "SELECT A.id, A.imatriculation, A.img, C.nom AS client, M.libelle AS marque FROM dbo.automobile AS A INNER JOIN dbo.client AS C ON A.id_client = C.id INNER JOIN dbo.marque AS M ON A.id_marque = M.id WHERE A.id='" + this.Request.Params.Get("id") + "'";
				DataTable dataTable = new DataTable();
				cmd.Connection = cnn;
				dataTable.Load(cmd.ExecuteReader());

				this.Image.Src = dataTable.Rows[0]["img"].ToString();
				this.plaque.Text = dataTable.Rows[0]["imatriculation"].ToString();
				this.proprio.InnerHtml = dataTable.Rows[0]["client"].ToString();
				this.marque.InnerHtml = dataTable.Rows[0]["marque"].ToString();
				
				this.grille.Attributes.CssStyle.Add("display", "none");
                this.HiddenID.Value = dataTable.Rows[0]["id"].ToString();

                fillInterventions();
				fillContrats();
			}
			else
			{
				fillGrid();
				this.contrat.Attributes.CssStyle.Add("display", "none");
				this.historique.Attributes.CssStyle.Add("display", "none");
				this.information.Attributes.CssStyle.Add("display", "none");
				
			}
		}

		protected void fillGrid()
		{
			cnn = new SqlConnection(connectionString);
			cnn.Open();
			SqlCommand cmd = new SqlCommand();
			cmd.CommandText = "SELECT A.id, A.imatriculation, C.nom AS client, M.libelle AS marque FROM dbo.automobile AS A INNER JOIN dbo.client AS C ON A.id_client = C.id INNER JOIN dbo.marque AS M ON A.id_marque = M.id;";
			DataTable dataTable = new DataTable();
			cmd.Connection = cnn;
			dataTable.Load(cmd.ExecuteReader());

			HtmlGenericControl temp;
			for (int i = 0; i < dataTable.Rows.Count; i++)
			{
				temp = new HtmlGenericControl();
				temp.InnerHtml = "<tr><td>" + "<a href='auto.aspx?id=" + dataTable.Rows[i]["id"] + "'>"+ dataTable.Rows[i]["imatriculation"] +"</a></td>"; 
				temp.InnerHtml +=    "<td>" + dataTable.Rows[i]["client"]            + "</td>";
				temp.InnerHtml +=    "<td>" + dataTable.Rows[i]["marque"]         + "</td>";
				temp.InnerHtml +=    "<td><a href='#'>Supprimer</a></td>";
				temp.InnerHtml += "</tr>";
				this.autobody.Controls.Add(temp);
			}
			cnn.Close();
		}

		protected void fillInterventions()
		{
			cnn = new SqlConnection(connectionString);
			cnn.Open();
			SqlCommand cmd = new SqlCommand();
			cmd.Connection = cnn;
			DataTable dataTable = new DataTable();
			cmd.CommandText = "SELECT P.id AS identifiant, C.id AS contrat, P.date AS date, P.duree FROM dbo.automobile AS A INNER JOIN dbo.auto_contrat AS AC ON A.id = AC.id_auto INNER JOIN dbo.contrat AS C ON AC.id_contrat = C.id INNER JOIN dbo.planification AS P ON P.id_contrat = C.id WHERE A.id='" + this.Request.Params.Get("id") + "';";
			dataTable.Load(cmd.ExecuteReader());

			HtmlGenericControl temp;
			for (int i = 0; i < dataTable.Rows.Count; i++)
			{
				temp = new HtmlGenericControl();
				temp.InnerHtml = "<tr><td>OOXX12</td><td>En vigeur</td> <td>Vidange</td> <td>21/08/1999</td> <td>21/08/2016</td></tr>";
				this.contratbody.Controls.Add(temp);
			}
			cnn.Close();
		}

		protected void fillContrats()
		{
			cnn = new SqlConnection(connectionString);
			cnn.Open();
			SqlCommand cmd = new SqlCommand();
			cmd.Connection = cnn;
			DataTable dataTable = new DataTable();
			cmd.CommandText = "SELECT C.id, type_contrat, debut, fin FROM dbo.contrat AS C INNER JOIN dbo.auto_contrat AS AC ON C.id = AC.id_contrat INNER JOIN dbo.automobile AS A ON AC.id_auto = A.id WHERE A.id='" + this.Request.Params.Get("id") + "';";
			dataTable.Load(cmd.ExecuteReader());
			HtmlGenericControl temp;
			for (int i = 0; i < dataTable.Rows.Count; i++)
			{
				temp = new HtmlGenericControl();
				temp.InnerHtml = "<tr><td>OOXX12</td><td>En vigeur</td> <td>Vidange</td> <td>21/08/1999</td> <td>21/08/2016</td></tr>";
				this.contratbody.Controls.Add(temp);
			}
			cnn.Close();
		}

        protected void Button1_Click(object sender, EventArgs e)
        {
            cnn = new SqlConnection(connectionString);
            cnn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cnn;
            int idAuto = Convert.ToInt32(this.HiddenID.Value);
            cmd.CommandText = "UPDATE [dbo].[automobile] " +
                                "SET imatriculation ='" + this.imatriculation.Value + "'" +
                                ", id_client='" + this.proprietaire.Value + "'" + 
                                ", id_marque='" + this.marqueInput.Value + "'" + 
                                "' WHERE id=" + idAuto;

            cmd.ExecuteNonQuery();
            cnn.Close();
        }
    }
}