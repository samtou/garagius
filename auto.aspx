﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GMAO.Master" AutoEventWireup="true" CodeBehind="auto.aspx.cs" Inherits="WebApplication3.WebForm3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	<div class="container">
		<div class="location">
			<asp:HyperLink ID="HyperLink1" runat="server">Acceuil</asp:HyperLink> / Automobiles
			<asp:Label ID="Label1" runat="server" Text=""></asp:Label>
		</div>
		
		<div id="information" runat="server" class="row">
			<div class="col-lg-6 row">
				<div class="col-lg-12">
					<img ID="Image" runat="server" style="display:block; width:100px; height:100px;" />
					<asp:Label ID="plaque" runat="server" Text=""></asp:Label>
				</div>
				<div class="col-lg-12">
					<span>PROPRIETAIRE : </span>  <span id="proprio" runat="server"></span>
				</div>
				<div class="col-lg-12">
					<span>MARQUE : </span>  <span id="marque" runat="server"></span>
				</div>
			</div>
			<div class="col-lg-6">
				<form id="form1" runat="server">
                    <asp:HiddenField ID="HiddenID" runat="server" />
					<div>
                        <asp:Label ID="Label2" runat="server" Text="Imatriculation"></asp:Label>
                        <input class="form-control" id="imatriculation" type="text" runat="server" />
					</div>
					<div>
                        <asp:Label ID="Label3" runat="server" Text="Proprietaire"></asp:Label>
                        <input class="form-control" id="proprietaire" type="text" runat="server" />
					</div>
					<div>
                        <asp:Label ID="Label4" runat="server" Text="Marque"></asp:Label>
                        <input class="form-control" id="marqueInput" type="text" runat="server" />
					</div>  
                    <asp:Button CssClass="btn btn-dark btn-lg" ID="Button1" runat="server" Text="Modifier" OnClick="Button1_Click" />
				</form>
			</div>

		</div>

		<div id="contrat" runat="server">
			<h2>Contrats</h2>
			<div class="table-responsive">
				<table class="table table-striped table-sm">
					<thead>
						<tr>
							<th>Idantifiant</th>
							<th>Etat</th>
							<th>Type de contrat</th>
							<th>Debut</th>
							<th>Fin</th>
							<th>Intervalle</th>
						</tr>
					</thead>
					<tbody id="contratbody" runat="server">

					</tbody>
				</table>
			</div>
		</div>

		<div id="historique" runat="server">
			<h2>Interventions</h2>
			<div class="table-responsive">
				<table class="table table-striped table-sm">
					<thead>
						<tr>
							<th>Identifiant</th>
							<th>Contrat</th>
							<th>Date</th>
							<th>Debut</th>
						</tr>
					</thead>
					<tbody id="histbody" runat="server">

					</tbody>
				</table>
			</div>
		</div>

		<div class="table-responsive" id="grille" runat="server">
			<table class="table table-striped table-sm">
				<thead>
					<tr>
						<th>Imatriculation</th>
						<th>Client</th>
						<th colspan="2">Marque</th>
					</tr>
				</thead>
				<tbody id="autobody" runat="server">

				</tbody>
			</table>
		</div>
		
	</div>
</asp:Content>
