﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace WebApplication3
{
    public partial class WebForm1 : System.Web.UI.Page
    {
    	SqlConnection cnn;
        string connectionString = "Data Source=localhost;Initial Catalog=GMAO; User ID=admin; Password=Asse1254; Integrated Security=SSPI;";
        
        protected void Page_Load(object sender, EventArgs e)
        {
        	if (this.Request.Params.Get("id") != null)
			{
				cnn = new SqlConnection(connectionString);
				cnn.Open();
				SqlCommand cmd = new SqlCommand();
				cmd.CommandText = "SELECT C.id, C.nom, C.telephone, C.email  FROM dbo.client AS C WHERE C.id=" + this.Request.Params.Get("id");
				DataTable dataTable = new DataTable();
				cmd.Connection = cnn;
				dataTable.Load(cmd.ExecuteReader());

				this.nom_label.InnerHtml = dataTable.Rows[0]["nom"].ToString();
				this.telephone_label.InnerHtml = dataTable.Rows[0]["telephone"].ToString();
				this.email_label.InnerHtml = dataTable.Rows[0]["email"].ToString();
				
				this.liste_client.Attributes.CssStyle.Add("display", "none");
                this.HiddenID.Value = dataTable.Rows[0]["id"].ToString();

                fillAutos();
				fillContrats();
			}
			else
			{
				fillGrid();
				this.contrat.Attributes.CssStyle.Add("display", "none");
				this.historique.Attributes.CssStyle.Add("display", "none");
				this.information.Attributes.CssStyle.Add("display", "none");
				
			}
        }

        protected void fillGrid()
		{
			cnn = new SqlConnection(connectionString);
			cnn.Open();
			SqlCommand cmd = new SqlCommand();
			cmd.CommandText = "SELECT C.id, C.nom, C.telephone, C.email  FROM dbo.client AS C";
			DataTable dataTable = new DataTable();
			cmd.Connection = cnn;
			dataTable.Load(cmd.ExecuteReader());

			HtmlGenericControl temp;
			for (int i = 0; i < dataTable.Rows.Count; i++)
			{
				temp = new HtmlGenericControl();
				temp.InnerHtml = "<tr><td>" + "<a href='auto.aspx?id=" + dataTable.Rows[i]["id"] + "'>"+ dataTable.Rows[i]["nom"] +"</a></td>"; 
				temp.InnerHtml +=    "<td>" + dataTable.Rows[i]["telephone"]            + "</td>";
				temp.InnerHtml +=    "<td>" + dataTable.Rows[i]["email"]         + "</td>";
				temp.InnerHtml +=    "<td><a href='#'>Supprimer</a></td>";
				temp.InnerHtml += "</tr>";
				this.listbody.Controls.Add(temp);
			}
			cnn.Close();
		}

		protected void fillAutos()
		{
			cnn = new SqlConnection(connectionString);
			cnn.Open();
			SqlCommand cmd = new SqlCommand();
			cmd.Connection = cnn;
			DataTable dataTable = new DataTable();
			cmd.CommandText = "SELECT A.id, A.imatriculation, M.libelle FROM dbo.automobile AS A INNER JOIN dbo.marque AS M ON A.id_marque = M.id WHERE A.id_client=" + this.Request.Params.Get("id");
			dataTable.Load(cmd.ExecuteReader());

			HtmlGenericControl temp;
			for (int i = 0; i < dataTable.Rows.Count; i++)
			{
				temp = new HtmlGenericControl();
				temp.InnerHtml = "<tr><td>OOXX12</td><td>En vigeur</td> <td>Vidange</td> <td>21/08/1999</td> <td>21/08/2016</td></tr>";
				this.contratbody.Controls.Add(temp);
			}
			cnn.Close();
		}

		protected void fillContrats()
		{
			cnn = new SqlConnection(connectionString);
			cnn.Open();
			SqlCommand cmd = new SqlCommand();
			cmd.Connection = cnn;
			DataTable dataTable = new DataTable();
			cmd.CommandText = "SELECT C.id, C.type_contrat, C.debut, C.fin, C.intervalle FROM dbo.contrat AS C INNER JOIN dbo.auto_contrat AS AC ON C.id = AC.id_contrat INNER JOIN dbo.automobile AS A ON AC.id_auto = A.id INNER JOIN dbo.type_contrat AS TC ON TC.id = C.type_contrat WHERE A.id_client =" + this.Request.Params.Get("id") + ";";
			dataTable.Load(cmd.ExecuteReader());

			HtmlGenericControl temp;
			for (int i = 0; i < dataTable.Rows.Count; i++)
			{
				temp = new HtmlGenericControl();
				temp.InnerHtml = "<tr><td>OOXX12</td><td>En vigeur</td> <td>Vidange</td> <td>21/08/1999</td> <td>21/08/2016</td></tr>";
				this.contratbody.Controls.Add(temp);
			}
			cnn.Close();
		}
    }
}