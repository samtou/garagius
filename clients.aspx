﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GMAO.Master" AutoEventWireup="true" CodeBehind="clients.aspx.cs" Inherits="WebApplication3.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	<div class="container">
		<div class="location">
			<a href="acceuil.aspx">Acceuil</a> / clients
			<!-- <asp:Label ID="Label1" runat="server" Text=""></asp:Label> -->
		</div>
		
		<div id="information" runat="server" class="row">
			<div class="col-lg-6 row">
				<div class="col-lg-12">
					<span>NOM : </span>  <span id="nom_label" runat="server"></span>
				</div>
				<div class="col-lg-12">
					<span>TELEPHONE : </span>  <span id="telephone_label" runat="server"></span>
				</div>
				<div class="col-lg-12">
					<span>EMAIL : </span>  <span id="email_label" runat="server"></span>
				</div>
			</div>
			<div class="col-lg-6">
				<form id="form1" runat="server">
					<asp:HiddenField ID="HiddenID" runat="server" />
					<div>
						<asp:Label ID="Label2" runat="server" Text="Nom"></asp:Label>
						<input class="form-control" id="nomInput" type="text" runat="server" />
					</div>
					<div>
						<asp:Label ID="Label3" runat="server" Text="Telephone"></asp:Label>
						<input class="form-control" id="telephoneInput" type="text" runat="server" />
					</div>
					<div>
						<asp:Label ID="Label4" runat="server" Text="Marque"></asp:Label>
						<input class="form-control" id="emailInput" type="text" runat="server" />
					</div>  
					<asp:Button CssClass="btn btn-dark btn-lg" ID="update" runat="server" Text="Modifier"  />
				</form>
			</div>

		</div>

		<div id="contrat" runat="server">
			<h2>Contrats</h2>
			<div class="table-responsive">
				<table class="table table-striped table-sm">
					<thead>
						<tr>
							<th>Idantifiant</th>
							<th>Etat</th>
							<th>Type de contrat</th>
							<th>Debut</th>
							<th>Fin</th>
							<th>Intervalle</th>
						</tr>
					</thead>
					<tbody id="contratbody" runat="server">

					</tbody>
				</table>
			</div>
		</div>

		<div id="historique" runat="server">
			<h2>Automobiles</h2>
			<div class="table-responsive">
				<table class="table table-striped table-sm">
					<thead>
						<tr>
							<th>Identifiant</th>
							<th>Contrat</th>
							<th>Date</th>
							<th>Debut</th>
						</tr>
					</thead>
					<tbody id="histbody" runat="server">

					</tbody>
				</table>
			</div>
		</div>

		<div class="table-responsive" id="liste_client" runat="server">
			<table class="table table-striped table-sm">
				<thead>
					<tr>
						<th>Nom</th>
						<th>telephone</th>
						<th colspan="2">Email</th>
					</tr>
				</thead>
				<tbody id="listbody" runat="server">
					
				</tbody>
			</table>
		</div>
		
	</div>
</asp:Content>
