﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="auth2.aspx.cs" Inherits="WebApplication3.auth2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title></title>
  <link rel="stylesheet" href="statics/css/bootstrap.css"/>
  <link rel="stylesheet" href="statics/css/font-awesome.css"/>
  <link rel="stylesheet" href="statics/css/signin.css"/>

  <script src="statics/js/jquery-1.3.2-vsdoc2.js"></script>
  <script src="statics/js/jquery-1.3.2.js"></script>
  <script src="statics/js/jquery.validate.js"></script>
</head>
<body>
    
    <asp:Label ID="msgError" runat="server" Text=""></asp:Label>
    <form id="form1" runat="server" class="form-signin">
      <img class="mb-4" src="https://getbootstrap.com/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72"/>
        <div class="alert-info logo" style="line-height: 85px; text-align: center; font-size: 51px;">GARAGIUS</div>
      <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
      <label for="username" class="sr-only">Username</label>
      <input id="username" runat="server" type="text" class="form-control" placeholder="Username" required autofocus />
      <label for="password" class="sr-only">Password</label>
      <input type="password" id="password" runat="server" class="form-control" placeholder="Password" required/>
      <div class="checkbox mb-3">
        <label>
          <input type="checkbox" value="remember-me"/> Remember me
        </label>
      </div>
        <asp:Button ID="signin" CssClass="btn btn-lg btn-primary btn-block" type="submit" runat="server" Text="Sign in" OnClick="signin_Click" />
      <p class="mt-5 mb-3 text-muted">&copy; 2017-2018</p>
    </form>
</body>
</html>
